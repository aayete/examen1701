<?php include 'views/header.php'; ?>
<main>

    <h1>Edición de producto</h1>

    <form method="post" action="/product/update/<?php echo $product->id; ?>">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $product->nombre ?>">
        <br>

        <label>Apellidos</label>
        <input type="text" name="precio" value="<?php echo $product->precio ?>">
        <br>

        <label>F. Nacimiento</label>
        <input type="text" name="fecha" value="<?php echo $product->fecha ?>">
        <br>

        <label>Tipo</label>
            <select name="id_tipo">
                <?php foreach ($tipos as $tipo): ?>
                    <option value="<?php echo $tipo->id; ?>"> <?php echo $tipo->nombre; ?></option>
                <?php endforeach ?>
            </select>
        <br>

        <input type="submit" value="Guardar cambios">
        <br>

    </form>
</main>
<?php include 'views/footer.php'; ?>
