<?php require 'views/header.php'; ?>
<main>

        <h1>Lista de Productos</h1>

        <table>
            <tr>
                <th>id</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Fecha</th>
                <th>Tipo</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><?php echo $product->id ?></td>
                <td><?php echo $product->nombre ?></td>
                <td><?php echo $product->precio ?></td>
                <?php
                $old_date = date($product->fecha);
                $old_date_timestamp = strtotime($old_date);
                $new_date = date('d-m-Y', $old_date_timestamp);
                ?>
                <td><?php echo $new_date ?></td>
                <td><?php echo $product->id_tipo ?></td>

                <td>
                    <a href="/product/edit/<?php echo $product->id ?>">Editar</a>
                    <a href="/product/remember/<?php echo $product->id ?>">Recordar</a>
                </td>
            </tr>
            <?php endforeach ?>
        </table>


        <p>
            <a href="/product/create">nuevo</a>
        </p>

</main>
<?php require 'views/footer.php'; ?>
