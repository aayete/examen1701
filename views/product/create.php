<?php require 'views/header.php'; ?>

        <h1>Producto nuevo</h1>

        <form method="post" action="/product/store">
            <label>Id</label>
            <input type="text" name="id"><br>
            <label>Nombre</label>
            <input type="text" name="nombre"><br>
            <label>Precio</label>
            <input type="text" name="precio"><br>
            <label>Fecha</label>
            <input type="date" name="fecha"><br>

            <label>Tipo</label>
            <select name="id_tipo">
                <?php foreach ($tipos as $tipo): ?>
                    <option value="<?php echo $tipo->id; ?>"> <?php echo $tipo->nombre; ?></option>
                <?php endforeach ?>
            </select>
            <br>

            <input type="submit" value="Nuevo">
        </form>
<?php require 'views/footer.php'; ?>
