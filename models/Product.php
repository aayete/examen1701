<?php

require_once('Tipo.php');
/**
*
*/
class Product
{
    public $id;
    public $title;
    public $author;
    public $pages;
    public $cathegory_id;

    function __construct()
    {

    }

    public function connect()
    {
        $dsn = 'mysql:dbname=examen1701;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';

        try {
            $db = new PDO($dsn, $usuario, $contraseña, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die ('Falló la conexión: ' . $e->getMessage());
        }
        return $db;
    }

    public static function all()
    {

        $db = Product::connect();

        $stmt = $db->prepare("SELECT * FROM producto");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');

        $results = $stmt->fetchAll();
        return $results;
    }

    public static function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id=:id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(":id", $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = Product::connect();
        $sql = "update producto set nombre=:nombre, precio=:precio, fecha=:fecha, id_tipo=:id_tipo where id=:id";
        $query = $db->prepare($sql);
        $query->bindValue(":nombre", $this->nombre);
        $query->bindValue(":precio", $this->precio);
        $query->bindValue(":fecha", $this->fecha);
        $query->bindValue(":id_tipo", $this->id_tipo);
        $query->bindValue(":id", $this->id);
        $result = $query->execute();
    }


    public function storeone()
    {
        $db = Product::connect();

        $sql = "INSERT INTO producto(id, nombre, precio, fecha, id_tipo) VALUES(?, ?, ?, ?, ?)";

        $query = $db->prepare($sql);
        $query->bindParam(1, $this->id);
        $query->bindParam(2, $this->nombre);
        $query->bindParam(3, $this->precio);
        $query->bindParam(4, $this->fecha);
        $query->bindParam(5, $this->id_tipo);

        return $query->execute();
    }

    public function tipo()
    {
        if (!isset($this->tipo)) {
            $this->tipo = Tipo::find($this->id_tipo);
        }
        return $this->tipo;
    }
    public function __get($nombre)
    {
        return $this->$nombre();
    }
}
