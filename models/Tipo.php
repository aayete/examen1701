<?php

/**
*
*/
require_once('app/Model.php');
require_once('Product.php');

class Tipo extends Model
{
    public $id;
    public $nombre;

    function __construct()
    {
    }

    public function find($id)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM tipo WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Tipo');
        return $stmt->fetch();
    }

    public static function all()
    {
        $db = Product::connect();

        $stmt = $db->prepare("SELECT * FROM tipo");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Tipo');

        $results = $stmt->fetchAll();
        return $results;
    }


}
