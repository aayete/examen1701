<?php

/**
*
*/
require_once("models/Product.php");

class ProductController
{

    function __construct()
    {
    }

    public function index()
    {

        $products = product::all();
        require("views/product/index.php");
    }

    public function create()
    {
        $tipos = Tipo::all();
        require("views/product/create.php");
    }

    public function store()
    {
        $product = new Product();
        $product->id = $_POST['id'];
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $now = new DateTime($_POST['fecha']);
        $timestring = $now->format('Y-m-d h:i:s');
        $product->fecha = $timestring;
        $product->id_tipo = $_POST['id_tipo'];
        $product->storeone();
        header('location: index');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $tipos = Tipo::all();
        require("views/product/edit.php");
    }
    public function update($id)
    {
        $product = Product::find($id);
        $product->nombre = $_POST['nombre'];
        $product->precio = $_POST['precio'];
        $product->fecha = $_POST['fecha'];
        $product->id_tipo = $_POST['id_tipo'];
        $product->save();
        header('location:../index');
    }

    public function remember($id)
    {
        $product = Product::find($id);
        $_SESSION['product'] [] = $product->nombre;
        header('Location: /product');
    }

}
